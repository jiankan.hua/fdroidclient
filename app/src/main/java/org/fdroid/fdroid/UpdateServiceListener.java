package org.fdroid.fdroid;

import android.content.Context;

import org.fdroid.index.v1.IndexUpdateListener;

class UpdateServiceListener implements IndexUpdateListener {

    private final Context context;
    private final String canonicalUrl;

    UpdateServiceListener(Context context, String canonicalUrl) {
        this.context = context;
        this.canonicalUrl = canonicalUrl;
    }

    @Override
    public void onDownloadProgress(long bytesRead, long totalBytes) {
        UpdateService.reportDownloadProgress(context, canonicalUrl, bytesRead, totalBytes);
    }

    @Override
    public void onStartProcessing() {
        UpdateService.reportProcessingAppsProgress(context, canonicalUrl, 0, 0);
    }
}

package org.fdroid.fdroid.net;

import android.content.ContentResolver;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.fdroid.database.Repository;
import org.fdroid.download.DownloadRequest;
import org.fdroid.download.Downloader;
import org.fdroid.download.HttpDownloader;
import org.fdroid.download.HttpManager;
import org.fdroid.download.Mirror;
import org.fdroid.fdroid.FDroidApp;
import org.fdroid.fdroid.Utils;

import java.io.File;
import java.io.IOException;
import java.net.Proxy;
import java.util.List;

import info.guardianproject.netcipher.NetCipher;

public class DownloaderFactory extends org.fdroid.download.DownloaderFactory {

    private static final String TAG = "DownloaderFactory";
    // TODO move to application object or inject where needed
    public static final DownloaderFactory INSTANCE = new DownloaderFactory();
    public static final HttpManager HTTP_MANAGER =
            new HttpManager(Utils.getUserAgent(), FDroidApp.queryString, NetCipher.getProxy());

    @NonNull
    @Override
    public Downloader create(Repository repo, @NonNull Uri uri, @NonNull File destFile) throws IOException {
        List<Mirror> mirrors = repo.getMirrors();
        return create(repo, mirrors, uri, destFile, null);
    }

    @NonNull
    @Override
    protected Downloader create(@NonNull Repository repo, @NonNull List<Mirror> mirrors, @NonNull Uri uri,
                                @NonNull File destFile, @Nullable Mirror tryFirst) throws IOException {
        Downloader downloader;

        String scheme = uri.getScheme();
        if (BluetoothDownloader.SCHEME.equals(scheme)) {
            downloader = new BluetoothDownloader(uri, destFile);
        } else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            downloader = new TreeUriDownloader(uri, destFile);
        } else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            downloader = new LocalFileDownloader(uri, destFile);
        } else {
            String path = uri.toString().replace(repo.getAddress(), "");
            Utils.debugLog(TAG, "Using suffix " + path + " with mirrors " + mirrors);
            Proxy proxy = NetCipher.getProxy();
            DownloadRequest request =
                    new DownloadRequest(path, mirrors, proxy, repo.getUsername(), repo.getPassword(), tryFirst);
            downloader = new HttpDownloader(HTTP_MANAGER, request, destFile);
        }
        return downloader;
    }
}

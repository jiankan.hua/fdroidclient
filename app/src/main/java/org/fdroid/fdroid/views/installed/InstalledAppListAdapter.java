package org.fdroid.fdroid.views.installed;

import android.view.View;
import android.view.ViewGroup;

import org.fdroid.database.AppListItem;
import org.fdroid.fdroid.R;
import org.fdroid.fdroid.data.App;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class InstalledAppListAdapter extends RecyclerView.Adapter<InstalledAppListItemController> {

    protected final AppCompatActivity activity;

    private final List<AppListItem> items = new ArrayList<>();

    protected InstalledAppListAdapter(AppCompatActivity activity) {
        this.activity = activity;
    }

    @NonNull
    @Override
    public InstalledAppListItemController onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = activity.getLayoutInflater().inflate(R.layout.installed_app_list_item, parent, false);
        return new InstalledAppListItemController(activity, view);
    }

    @Override
    public void onBindViewHolder(@NonNull InstalledAppListItemController holder, int position) {
        AppListItem item = items.get(position);
        holder.bindModel(new App(item), null, null);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setApps(@NonNull List<AppListItem> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    @Nullable
    App getItem(int position) {
        AppListItem item = items.get(position);
        return item == null ? null : new App(item);
    }
}
